package paulacr.net.android.listener;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import paulacr.net.android.interfaces.OnScrollMoreCalled;

/**
 * Created by paularosa on 2/28/16.
 */
public class EndlessScrollListener extends RecyclerView.OnScrollListener {

    private boolean loading = true;
    private OnScrollMoreCalled listener;

    private int total = 0;
    private int currentPage = 0;
    private LinearLayoutManager layoutManager;

    public EndlessScrollListener(LinearLayoutManager manager) {
        this.layoutManager = manager;
        this.listener = null;
    }

    public void setListener(OnScrollMoreCalled instance) {
        this.listener = instance;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        int visibleItensTotal = recyclerView.getChildCount();
        int totalItens = layoutManager.getItemCount();
        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
        int visibleThreshold = 5;

        if(loading && (totalItens > total)) {
            loading = false;
            total = totalItens;
        }

        if(!loading && (totalItens - visibleItensTotal) <= (firstVisibleItem + visibleThreshold)) {
            loading = true;
            Log.i("Log endlessScroll", "--> calling endless scroll");
            currentPage++;
            listener.scrollMore(currentPage);
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
    }
}
