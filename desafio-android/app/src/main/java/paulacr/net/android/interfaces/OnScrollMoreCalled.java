package paulacr.net.android.interfaces;

/**
 * Created by paularosa on 2/28/16.
 */
public interface OnScrollMoreCalled {

    void scrollMore(int page);
}
