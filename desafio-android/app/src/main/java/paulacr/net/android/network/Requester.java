package paulacr.net.android.network;

import paulacr.net.android.interfaces.RequesterInterface;
import paulacr.net.android.model.Repositories;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by paularosa on 2/28/16.
 */
public class Requester {

    RequesterInterface listener;

    public Requester() {
        this.listener = null;
    }

    public void setListener(RequesterInterface listener) {
        this.listener = listener;
    }

    public void performRequest(String language, int page) {
        RestApi.GithubApi service = RestApi.getClient();
        Call<Repositories> call = service.requestRepositorie(
                language,
                "starts",
                String.valueOf(page));

        call.enqueue(repositoryCallback);
    }


    Callback<Repositories> repositoryCallback = new Callback<Repositories>() {
        @Override
        public void onResponse(Call<Repositories> call, Response<Repositories> response) {
            listener.onSuccess(call, response);
        }

        @Override
        public void onFailure(Call<Repositories> call, Throwable t) {
            listener.onFailure(call, t);
        }
    };

}
