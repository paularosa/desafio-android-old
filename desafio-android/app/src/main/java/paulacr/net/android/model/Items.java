package paulacr.net.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paularosa on 2/27/16.
 */
public class Items {

    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("forks_count")
    long forksCount;
    @SerializedName("stargazers_count")
    long starsCount;
    @SerializedName("has_issues")
    private Boolean issues;
    @SerializedName("owner")
    private Owner owner;

    private List<Issues> issuesState = new ArrayList<Issues>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getForksCount() {
        return forksCount;
    }

    public void setForksCount(long forksCount) {
        this.forksCount = forksCount;
    }

    public long getStarsCount() {
        return starsCount;
    }

    public void setStarsCount(long starsCount) {
        this.starsCount = starsCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Boolean getIssues() {
        return issues;
    }

    public void setIssues(Boolean issues) {
        this.issues = issues;
    }
}
