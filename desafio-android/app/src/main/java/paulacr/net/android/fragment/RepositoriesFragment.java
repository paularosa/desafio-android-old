package paulacr.net.android.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import paulacr.net.android.R;
import paulacr.net.android.adapter.RepositoriesAdapter;
import paulacr.net.android.interfaces.OnScrollMoreCalled;
import paulacr.net.android.interfaces.RequesterInterface;
import paulacr.net.android.listener.EndlessScrollListener;
import paulacr.net.android.model.Items;
import paulacr.net.android.model.Repositories;
import paulacr.net.android.network.Requester;
import paulacr.net.android.utils.RecyclerviewItemDecorator;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by paularosa on 2/27/16.
 */
public class RepositoriesFragment extends Fragment implements OnScrollMoreCalled, RequesterInterface {

    @Bind(R.id.repositoriesRecyclerview)
    RecyclerView repositoriesRecyclerview;

    public static final String TAG = "Repositories_fragment";
    private Repositories itens;
    private ProgressDialog progress;
    private LinearLayoutManager manager;
    private RepositoriesAdapter adapter;
    private ArrayList<Items> itemsList;
    private Requester requester;

    public static RepositoriesFragment newInstance() {
        return new RepositoriesFragment();
    }

    // ---------------------------------------------------------
    // LifeCycle
    // ---------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.repositories_fragment_layout, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showProgress();
        createRepositoresRecyclerview();
        requester = new Requester();
        requester.setListener(this);
        requester.performRequest("Java", 6);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    // ---------------------------------------------------------
    // Setup Recyclerview
    // ---------------------------------------------------------

    private void createRepositoresRecyclerview() {
        itemsList = new ArrayList<>();

        manager = new LinearLayoutManager(getActivity());
        adapter = new RepositoriesAdapter(itemsList);

        repositoriesRecyclerview.setLayoutManager(manager);
        repositoriesRecyclerview.setAdapter(adapter);
        repositoriesRecyclerview.addItemDecoration(new RecyclerviewItemDecorator(getActivity(), 1));

        EndlessScrollListener scrollListener = new EndlessScrollListener(manager);
        scrollListener.setListener(this);
        repositoriesRecyclerview.addOnScrollListener(scrollListener);
    }

    // ---------------------------------------------------------
    // Flow control
    // ---------------------------------------------------------
    private void showProgress() {
        progress = new ProgressDialog(getActivity());
        progress.setMessage("buscando dados...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();
    }

    private void hideProgress() {
        progress.dismiss();
    }

    // ---------------------------------------------------------
    // Interfaces Callback
    // ---------------------------------------------------------

    @Override
    public void scrollMore(int page) {
        showProgress();
        requester.performRequest("Java", page);

    }

    @Override
    public void onSuccess(Call<Repositories> call, Response<Repositories> response) {
        if(response.isSuccess()) {
            //Verify the last adapter position, to add items after it.
            int currentSize = adapter.getItemCount();

            Repositories result = response.body();
            itemsList.addAll(result.getItems());
            adapter.notifyItemRangeChanged(currentSize, itemsList.size() - 1);

            hideProgress();
        }
    }

    @Override
    public void onFailure(Call<Repositories> call, Throwable t) {
        hideProgress();
        //Toast
    }
}
