package paulacr.net.android.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import paulacr.net.android.model.Repositories;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by paularosa on 2/27/16.
 */
public class RestApi {

    public static Retrofit REST_ADAPTER;
    public static final String ENDPOINT_URL = "search/repositories";
    public static final String BASE_URL = "https://api.github.com/";
    public static GithubApi githubApi;

    public static GithubApi getClient() {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        if(REST_ADAPTER == null) {
            REST_ADAPTER = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            githubApi = REST_ADAPTER.create(GithubApi.class);
        }
        return githubApi;
    }

    public interface GithubApi {
        @GET(ENDPOINT_URL)
        Call<Repositories> requestRepositorie(
                @Query("q") String language,
                @Query("sort") String start,
                @Query("page") String page
        );
    }
}
