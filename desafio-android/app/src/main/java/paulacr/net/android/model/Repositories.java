package paulacr.net.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by paularosa on 2/27/16.
 */
public class Repositories {

    @SerializedName("total_count")
    private Integer totalCount;
    @SerializedName("incomplete_results")
    private boolean isIncompleteResults;
    @SerializedName("items")
    private ArrayList<Items> items = new ArrayList<>();

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return isIncompleteResults;
    }

    public void setIsIncompleteResults(boolean isIncompleteResults) {
        this.isIncompleteResults = isIncompleteResults;
    }

    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }
}
