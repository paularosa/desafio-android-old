package paulacr.net.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 2/27/16.
 */
public class Issues {

    @SerializedName("state")
    String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
