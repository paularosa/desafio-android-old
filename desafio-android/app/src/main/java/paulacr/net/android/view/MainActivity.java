package paulacr.net.android.view;

import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import paulacr.net.android.R;
import paulacr.net.android.fragment.RepositoriesFragment;
import paulacr.net.android.model.Repositories;
import paulacr.net.android.network.RestApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.navigationView)
    NavigationView navigationView;
    @Bind(R.id.drawer)
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupMenus();

    }

    @Override
    protected void onResume() {
        super.onResume();

        double metrics = getResources().getDisplayMetrics().density;
        Log.i("Log metrics", "-->" + metrics);

        changeFragment(RepositoriesFragment.newInstance());
    }

    private void setupMenus() {
        setupToolbar();
        setupActionBar();
        setupDrawer(navigationView);

    }

    private void setupActionBar() {
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.menu);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    private void setupDrawer(NavigationView navView) {
        if(navigationView != null) {
            navView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem item) {
                            item.setChecked(true);
                            drawerLayout.closeDrawers();
                            //Put the methods changefragment here
                            return true;
                        }
                    }
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void changeFragment(Fragment fragmentToShow) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragmentToShow)
                .addToBackStack(null)
                .commit();
    }

    // ---------------------------------------------------------
    // Request Data from Service
    // ---------------------------------------------------------

    private void getRepositories(String language, String page) {
        RestApi.GithubApi service = RestApi.getClient();
        Call<Repositories> call = service.requestRepositorie(
                language,
                "starts",
                page);

        call.enqueue(repositorieCallback);
    }

    Callback<Repositories> repositorieCallback = new Callback<Repositories>() {
        @Override
        public void onResponse(Call<Repositories> call, Response<Repositories> response) {
            Log.i("log response", "-->" + response);

            if(response.isSuccess()) {
                Repositories result = response.body();
                Log.i("log result", "--> " + result.getItems().get(0).getTitle());
            }
        }

        @Override
        public void onFailure(Call<Repositories> call, Throwable t) {

        }
    };

}
