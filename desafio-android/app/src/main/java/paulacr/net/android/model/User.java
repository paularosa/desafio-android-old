package paulacr.net.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 2/27/16.
 */
public class User {

    @SerializedName("name")
    String completeName;
    @SerializedName("location")
    String location;

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
