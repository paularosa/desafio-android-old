package paulacr.net.android.interfaces;

import paulacr.net.android.model.Repositories;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by paularosa on 2/28/16.
 */
public interface RequesterInterface {

    void onSuccess(Call<Repositories> call, Response<Repositories> response);

    void onFailure(Call<Repositories> call, Throwable t);
}
