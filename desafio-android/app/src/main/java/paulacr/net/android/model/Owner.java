package paulacr.net.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 2/27/16.
 */
public class Owner {

    @SerializedName("avatar_url")
    String avatarUrl;
    @SerializedName("login")
    String userName;
    @SerializedName("url")
    String userUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }
}
