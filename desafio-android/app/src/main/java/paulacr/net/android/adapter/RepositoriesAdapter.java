package paulacr.net.android.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import paulacr.net.android.R;
import paulacr.net.android.model.Items;
import paulacr.net.android.model.Owner;
import paulacr.net.android.utils.Utils;

/**
 * Created by paularosa on 2/27/16.
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>{

    private ArrayList<Items> repositories;

    public RepositoriesAdapter(ArrayList<Items> repositories) {
        this.repositories = repositories;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.repositoryName)
        TextView name;
        @Bind(R.id.repositoryDescription)
        TextView description;
        @Bind(R.id.repositoryForksCount)
        TextView forksCount;
        @Bind(R.id.repositoryStarsCount)
        TextView starsCount;
        @Bind(R.id.repositoryProfile)
        ImageView profileImage;
        @Bind(R.id.repositoryUsername)
        TextView userName;
        @Bind(R.id.repositoryCompleteName)
        TextView completeName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repositories_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Items item = repositories.get(position);
        Owner owner = item.getOwner();

        holder.name.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        holder.forksCount.setText(String.valueOf(item.getForksCount()));
        holder.starsCount.setText(String.valueOf(item.getStarsCount()));
        holder.userName.setText(owner.getUserName());
        holder.completeName.setText(owner.getUserName());

        //ImageCache with Picasso
        Uri uriProfileImage = new Utils().transformStringIntoUri(owner.getAvatarUrl());

        Picasso.with(holder.profileImage
                .getContext())
                .load(uriProfileImage)
                .into(holder.profileImage);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }
}
